# linux-c630

This is an Arch Linux PKGBUILD script to build a 64-bit
kernel for the Lenovo Yoga C630 from https://github.com/aarch64-laptops/linux/tree/laptops-5.11

## how to build

To cross-compile on your x86 machine, run:

```
export ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- CARCH=aarch64 && makepkg -As 
